import React, {FC} from "react";
import {BrowserRouter} from "react-router-dom"
import "./assets/skote/scss/theme.scss"
import AppRouter from "./router/AppRouter";


const App: FC = () => {
    return (
        <BrowserRouter>
            <AppRouter/>
        </BrowserRouter>
    );
};

export default App;
