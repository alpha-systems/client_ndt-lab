import {EmployeeState} from "../../types/Employee/employee";

export const initialState:EmployeeState =  {
    employees: [],
    loading: false,
    error: null
}