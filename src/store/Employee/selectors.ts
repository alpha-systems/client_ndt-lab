import {AppState} from "../index";
import {EmployeeState} from "../../types/Employee/employee";
import {IEmployee} from "../../types/Employee/IEmployee";

const employees = (state: AppState): EmployeeState => state.employeeSlice;
export const employeesListSelector = (state: AppState): IEmployee[] => employees(state).employees;
export const loadingSelector = (state: AppState): boolean => employees(state).loading
export const errorsSelector = (state: AppState): string | null => employees(state).error