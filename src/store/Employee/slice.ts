import {createSlice} from "@reduxjs/toolkit";
import {initialState} from "./initialState";

const employeeSlice = createSlice({
    name: 'employee',
    initialState,
    reducers: {
        employeeStart (state) {
            state.loading = true;
        },

        employeeSuccess (state, action) {
            state.employees = action.payload;
            state.loading = false

        },
        employeeError (state, action) {
            state.loading = false;
            state.error = action.payload
        }
    },
});



export const {employeeStart,employeeSuccess, employeeError} = employeeSlice.actions


export default employeeSlice.reducer
