import {Dispatch,} from "@reduxjs/toolkit";
import EmployeeApi from "../../api/EmployeesApi";
import {IInitialEmployee} from "../../types/Employee/employee";
import {IHistory} from "../../types/Employee/IEmployee";
import {employeeError, employeeStart, employeeSuccess} from "./slice"
import {createBrowserHistory} from 'history';

const history = createBrowserHistory({forceRefresh:true});

interface IHistoryList{
    historyList: Array<IHistory>
}

export const fetchEmployees = () => async (dispatch: Dispatch) => {
    try {
        dispatch(employeeStart())
        const response = await EmployeeApi.getEmployeesList()
        dispatch(employeeSuccess(response.data))

    } catch (e) {

        dispatch(employeeError((e as Error).message))
    }
}



export const sendEmployee = (employee: IInitialEmployee) => async (dispatch: Dispatch) => {

    const dataEmployee: IHistoryList = {
        historyList: [employee]
    }

    try {
        dispatch(employeeStart())

        if (employee.photo){
            const responsePhoto = await EmployeeApi.uploadPhotoEmployee(employee.photo)

            dataEmployee.historyList[0].photoFileName = responsePhoto.data
            const responseEmployee = await EmployeeApi.addEmployee(dataEmployee)

            dispatch(employeeSuccess(responseEmployee.data))

            history.push("/employee/" + responseEmployee.data.employeeId)
        }
    } catch (e) {
        dispatch(employeeError((e as Error).message))
    }
}

