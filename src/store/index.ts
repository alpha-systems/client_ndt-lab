import {configureStore} from '@reduxjs/toolkit'
import {combineReducers} from 'redux'
import employeeSlice from './Employee/slice'


const reducer = combineReducers({
    employeeSlice
})

export const index = configureStore({
    reducer
})

export type AppDispatch = typeof index.dispatch;
export type AppState = ReturnType<typeof index.getState>