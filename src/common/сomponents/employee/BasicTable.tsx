import React from "react"
import {Table} from "reactstrap";
import {IEmployee} from "../../../types/Employee/IEmployee";

interface Props {
    employees: IEmployee[]
}


const BasicTable: React.FC<Props> = ({employees}: Props) => {

    const rows = employees.map((data: IEmployee, index: number) => {

        //TODO Как переименовать historyList через интерфейсы
        const employee = data.historyList[0]
        return (
            <tr key={data.employeeId}>
                <th scope="row">{index}</th>
                <td><b
                    style={{color: "#4458b8"}}>{employee.lastName} {employee.firstName} {employee.patronymic}</b>
                </td>
                <td>{}</td>
                <td>{}</td>
            </tr>
        );
    });

    return (
        <div className="table-responsive ">

            <Table className="table table-sm table-striped mb-0">

                <thead>
                <tr>
                    <th>#</th>
                    <th>ФИО сотрудника</th>
                    <th>Деятельность</th>
                    <th>Площадка</th>
                </tr>
                </thead>

                <tbody>
                {rows}
                </tbody>
            </Table>
        </div>
    )
}
export default BasicTable