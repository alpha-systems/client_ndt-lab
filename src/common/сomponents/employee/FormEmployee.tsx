import React, {FC} from "react";
import {Button, CardTitle, Row} from "reactstrap";
import {Form, Formik} from "formik";
import * as yup from "yup";
import {useDispatch} from "react-redux";


import {IInitialEmployee} from "../../../types/Employee/employee";
import FileUploader from "../ui/FileUploader";
import SelectInput from "../ui/SelectInput";
import {sendEmployee} from "../../../store/Employee/actions";
import NumberPhoneInput from "../ui/NumberPhoneInput";
import Input from "../ui/Input";
import dayjs from "dayjs";


interface IOveralls {
    clothingSize: string[];
    pants: string[];
    gloves: string[];
    shoes: string[];
    headdress: string[];
}

//Данный массив будет передаваться в selects размеров спецодежды
const clothesSizes: IOveralls = {
    clothingSize: ["40/XS", "42/XS", "44/S", "46/M", "48/M", "50/L", "52/XL", "54/XL", "56/XXL", "58/XXXL"],
    pants: ["88-92", "96-100", "104-108", "112-116", "120-124"],
    gloves: ["S", "M", "L", "XL", "XXL"],
    shoes: ["34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58"],
    headdress: ["57", "58", "59", "60", "61", "62"]
}

const initialEmployee: IInitialEmployee = {
    photo: null,
    firstName: "",
    patronymic: "",
    lastName: "",
    firstNameGenitive: "",
    patronymicGenitive: "",
    lastNameGenitive: "",
    firstNameDative: "",
    patronymicDative: "",
    lastNameDative: "",
    firstNameTransliteration: "",
    lastNameTransliteration: "",
    dateBirth: "",
    inn: "",
    gender: "",
    position: "",
    contractualEntity: "",
    phoneNumber1: "",
    phoneNumber2: "",
    email: "",
    passSeries: "",
    passNumber: "",
    passDateIssue: "",
    passIssuedBy: "",
    educationalGrounding: "",
    institutionOfLearning: "",
    speciality: "",
    addressPostcode: "",
    addressCountry: "",
    addressRegion: "",
    addressSettlement: "",
    addressStreet: "",
    addressHouse: "",
    addressBlock: "",
    addressFlat: "",
    ppiClothing: "",
    ppiHeaddress: "",
    ppiFootwear: "",
    ppiHeight: "",
    ppiPants: "",
    ppiGloves: "",
    dlCategory: "",
    beginDate: "",
    //TODO После создания аутентификации и авторизации изменить id пользователя
    naviCreateUser: 1
}

const FormEmployee: FC = () => {

    const dispatch = useDispatch();

    const saveEmployee = (employee: IInitialEmployee) => {

        const now = dayjs();

        employee.beginDate = now.format('-YYYY-MM-DDTHH:mm:ss')
        dispatch(sendEmployee(employee))
    }

    const validationSchema = yup.object().shape({
        photo: yup.mixed()
            .required('Фото должно быть загружено')
            .test('fileFormat', 'Недопустимый тип файла', (value) => {
                return value && ["image/jpg", "image/jpeg", "image/gif", "image/png"].includes(value.type);
            }),
        firstName: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        patronymic: yup.string()
            .typeError('В поле должна быть строка'),
        lastName: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        firstNameGenitive: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        patronymicGenitive: yup.string()
            .typeError('В поле должна быть строка'),
        lastNameGenitive: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        firstNameDative: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        patronymicDative: yup.string()
            .typeError('В поле должна быть строка'),
        lastNameDative: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено'),
        firstNameTransliteration: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено')
            .matches(/^[a-zA-Z]+$/, "Имя должно быть на английском"),
        lastNameTransliteration: yup.string()
            .typeError('В поле должна быть строка')
            .required('Поле должно быть заполнено')
            .matches(/^[a-zA-Z]+$/, "Фамилия должно быть на английском"),
        dateBirth: yup.string()
            .required('Поле должно быть заполнено')
            .matches(/(\d{4})-(\d{1,2})-(\d{1,2})/, "Дата должна содержать только числа"),
        gender: yup.string()
            .required('Поле должно быть заполнено')
            .matches(/((\W|^)Муж|Жен(\W|$))/, "Поле должно содержать пол"),
        inn: yup.string()
            .required('Поле должно быть заполнено')
            .matches(/^[0-9]+$/, "Инн должен содержать только числа")
            .min(12, 'Инн должен содержать только 12 знаков')
            .max(12, 'Инн должен содержать только 12 знаков'),
        phoneNumber1: yup.string()
            .required('Поле должно быть заполнено')
            .min(10, 'Номер телефона должен содержать от 10 до 14 знаков')
            .max(18, 'Номер телефона должен содержать от 10 до 14 знаков'),
        phoneNumber2: yup.string()
            .matches(/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?)/, "Номер телефона должен содержать только числа")
            .min(10, 'Номер телефона должен содержать от 10 до 14 знаков')
            .max(18, 'Номер телефона должен содержать от 10 до 14 знаков'),
        email: yup.string()
            .required('Поле должно быть заполнено')
            .email("Не корректно введена электронная почта"),
        passSeries: yup.string()
            .matches(/^[0-9]+$/, "Серия паспорта должна содержать только числа"),
        passNumber: yup.string()
            .required('Поле должно быть заполнено')
            .matches(/^[0-9]+$/, "Номер паспорта должен содержать только числа"),
        passIssuedBy: yup.string()
            .required('Поле должно быть заполнено'),
        passDateIssue: yup.string()
            .required('Поле должно быть заполнено')
            .matches(/(\d{4})-(\d{1,2})-(\d{1,2})/, "Дата должна содержать только числа"),
        position: yup.string()
            .required('Поле должно быть заполнено')
    })


    return (
        <Formik
            initialValues={initialEmployee}
            validateOnBlur
            validationSchema={validationSchema}
            onSubmit={(
                values: IInitialEmployee,
            ) => {
                saveEmployee(values)
            }}
        >

            {({
                  values,
                  errors,
                  touched,
                  handleBlur,
                  isValid,
                  dirty,
                  handleChange,
                  setFieldValue
              }) => (
                <Form>
                    {/*Блок личные данные*/}
                    <Row className="mb-4">

                        <CardTitle className="h4 ">Личные данные</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-4">
                                <FileUploader titleInput="Фото сотрудника"
                                              typeInput="file"
                                              size="form-control-sm"
                                              nameInput="photo"
                                              setValue={setFieldValue}
                                              valueInput={values.photo}
                                              onBlurInput={handleBlur}
                                              touchedInput={touched.photo}
                                              errorsInput={errors.photo}

                                />
                            </div>
                        </Row>

                        <Row className="mb-3">

                            <div className="col-lg-4">
                                <Input titleInput="Имя"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="firstName"
                                       valueInput={values.firstName}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.firstName}
                                       errorsInput={errors.firstName}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Имя в родительном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="firstNameGenitive"
                                       valueInput={values.firstNameGenitive}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.firstNameGenitive}
                                       errorsInput={errors.firstNameGenitive}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Имя в дательном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="firstNameDative"
                                       valueInput={values.firstNameDative}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.firstNameDative}
                                       errorsInput={errors.firstNameDative}
                                />
                            </div>
                        </Row>


                        <Row className="mb-3">

                            <div className="col-lg-4">
                                <Input titleInput="Отчество"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="patronymic"
                                       valueInput={values.patronymic}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.patronymic}
                                       errorsInput={errors.patronymic}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Отчество в родительном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="patronymicGenitive"
                                       valueInput={values.patronymicGenitive}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.patronymicGenitive}
                                       errorsInput={errors.patronymicGenitive}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Отчество в дательном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="patronymicDative"
                                       valueInput={values.patronymicDative}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.patronymicDative}
                                       errorsInput={errors.patronymicDative}
                                />
                            </div>

                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-4">
                                <Input titleInput="Фамилия"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="lastName"
                                       valueInput={values.lastName}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.lastName}
                                       errorsInput={errors.lastName}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Фамилия в родительном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="lastNameGenitive"
                                       valueInput={values.lastNameGenitive}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.lastNameGenitive}
                                       errorsInput={errors.lastNameGenitive}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Фамилия в дательном падеже"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="lastNameDative"
                                       valueInput={values.lastNameDative}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.lastNameDative}
                                       errorsInput={errors.lastNameDative}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-4">
                                <Input titleInput="Имя на английском языке"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="firstNameTransliteration"
                                       valueInput={values.firstNameTransliteration}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.firstNameTransliteration}
                                       errorsInput={errors.firstNameTransliteration}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Фамилия на английском языке"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="lastNameTransliteration"
                                       valueInput={values.lastNameTransliteration}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.lastNameTransliteration}
                                       errorsInput={errors.lastNameTransliteration}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <Input titleInput="Дата рождения"
                                       typeInput="date"
                                       size="form-control-sm"
                                       nameInput="dateBirth"
                                       valueInput={values.dateBirth}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.dateBirth}
                                       errorsInput={errors.dateBirth}
                                />
                            </div>

                            <div className="col-lg-2">
                                <SelectInput titleInput="Пол"
                                             options={["Муж", "Жен"]}
                                             size="form-control-sm"
                                             nameInput="gender"
                                             valueInput={values.gender}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.gender}
                                             errorsInput={errors.gender}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="ИНН"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="inn"
                                       valueInput={values.inn}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.inn}
                                       errorsInput={errors.inn}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-3">
                                <NumberPhoneInput titleInput="Личный номер телефона"
                                                  size="form-control-sm"
                                                  valueInput={values.phoneNumber1}
                                                  nameInput="phoneNumber1"
                                                  onBlurInput={handleBlur}
                                                  onChangeInput={handleChange}
                                                  touchedInput={touched.phoneNumber1}
                                                  errorsInput={errors.phoneNumber1}
                                />
                            </div>

                            <div className="col-lg-3">
                                <NumberPhoneInput titleInput="Рабочий номер телефона"
                                                  size="form-control-sm"
                                                  valueInput={values.phoneNumber2}
                                                  nameInput="phoneNumber2"
                                                  onBlurInput={handleBlur}
                                                  onChangeInput={handleChange}
                                                  touchedInput={touched.phoneNumber2}
                                                  errorsInput={errors.phoneNumber2}
                                />
                            </div>

                            <div className="col-lg-3">
                                <Input titleInput="Электронная почта"
                                       typeInput="email"
                                       size="form-control-sm"
                                       nameInput="email"
                                       valueInput={values.email}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.email}
                                       errorsInput={errors.email}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-4">
                                <Input titleInput="Наименование подрядной организации"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="contractualEntity"
                                       valueInput={values.contractualEntity}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.contractualEntity}
                                       errorsInput={errors.contractualEntity}
                                />
                            </div>
                        </Row>
                    </Row>

                    {/*Блок паспортные данные*/}
                    <Row className="mb-4">

                        <CardTitle className="h4">Паспортные данные</CardTitle>

                        <Row className="mb-3">

                            <div className="col-lg-2">
                                <Input titleInput="Серия паспорта"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="passSeries"
                                       valueInput={values.passSeries}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.passSeries}
                                       errorsInput={errors.passSeries}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Номер паспорта"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="passNumber"
                                       valueInput={values.passNumber}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.passNumber}
                                       errorsInput={errors.passNumber}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Кем выдан"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="passIssuedBy"
                                       valueInput={values.passIssuedBy}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.passIssuedBy}
                                       errorsInput={errors.passIssuedBy}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Дата выдачи"
                                       typeInput="date"
                                       size="form-control-sm"
                                       nameInput="passDateIssue"
                                       valueInput={values.passDateIssue}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.passDateIssue}
                                       errorsInput={errors.passDateIssue}
                                />
                            </div>
                        </Row>
                    </Row>

                    {/*Блок почтовый адрес*/}
                    <Row className="mb-4">

                        <CardTitle className="h4">Адрес</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <Input titleInput="Индекс"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressPostcode"
                                       valueInput={values.addressPostcode}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressPostcode}
                                       errorsInput={errors.addressPostcode}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <Input titleInput="Страна"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressCountry"
                                       valueInput={values.addressCountry}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressCountry}
                                       errorsInput={errors.addressCountry}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Регион"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressRegion"
                                       valueInput={values.addressRegion}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressRegion}
                                       errorsInput={errors.addressRegion}
                                />
                            </div>

                            <div className="col-lg-3">
                                <Input titleInput="Город / Населенный пункт"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressSettlement"
                                       valueInput={values.addressSettlement}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressSettlement}
                                       errorsInput={errors.addressSettlement}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <Input titleInput="Улица"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressStreet"
                                       valueInput={values.addressStreet}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressStreet}
                                       errorsInput={errors.addressStreet}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Номер дома"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressHouse"
                                       valueInput={values.addressHouse}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressHouse}
                                       errorsInput={errors.addressHouse}
                                />
                            </div>

                            <div className="col-lg-3">
                                <Input titleInput="Корпус / строение"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressBlock"
                                       valueInput={values.addressBlock}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressBlock}
                                       errorsInput={errors.addressBlock}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Номер квартиры"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="addressStreet"
                                       valueInput={values.addressFlat}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.addressFlat}
                                       errorsInput={errors.addressFlat}
                                />
                            </div>

                        </Row>
                    </Row>

                    {/*Блок Образование*/}
                    <Row className="mb-4">

                        <CardTitle className="h4">Образование</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-3">
                                <Input titleInput="Уровень образования"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="educationalGrounding"
                                       valueInput={values.educationalGrounding}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.educationalGrounding}
                                       errorsInput={errors.educationalGrounding}
                                />
                            </div>

                            <div className="col-lg-4">
                                <Input titleInput="Образовательное учреждение"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="institutionOfLearning"
                                       valueInput={values.institutionOfLearning}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.institutionOfLearning}
                                       errorsInput={errors.institutionOfLearning}
                                />
                            </div>

                            <div className="col-lg-3">
                                <Input titleInput="Специальность"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="speciality"
                                       valueInput={values.speciality}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.speciality}
                                       errorsInput={errors.speciality}
                                />
                            </div>

                        </Row>
                    </Row>

                    {/*Блок должность*/}
                    <Row className="mb-4">

                        <CardTitle className="h4">Должность</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-3">
                                <Input titleInput="Название должности"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="position"
                                       valueInput={values.position}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.position}
                                       errorsInput={errors.position}
                                />
                            </div>
                        </Row>
                    </Row>

                    {/*Блок размер спецодежды*/}
                    <Row className="mb-3">

                        <CardTitle className="h4">Размер спецодежды</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <SelectInput titleInput="Размер одежды"
                                             options={clothesSizes.clothingSize}
                                             size="form-control-sm"
                                             nameInput="ppiClothing"
                                             valueInput={values.ppiClothing}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.ppiClothing}
                                             errorsInput={errors.ppiClothing}
                                />
                            </div>

                            <div className="col-lg-2">
                                <SelectInput titleInput="Размер брюк"
                                             options={clothesSizes.pants}
                                             size="form-control-sm"
                                             nameInput="ppiPants"
                                             valueInput={values.ppiPants}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.ppiPants}
                                             errorsInput={errors.ppiPants}
                                />
                            </div>

                            <div className="col-lg-2">
                                <SelectInput titleInput="Размер перчаток"
                                             options={clothesSizes.gloves}
                                             size="form-control-sm"
                                             nameInput="ppiGloves"
                                             valueInput={values.ppiGloves}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.ppiGloves}
                                             errorsInput={errors.ppiGloves}
                                />
                            </div>
                        </Row>

                        <Row className="mb-3">
                            <div className="col-lg-2">
                                <SelectInput titleInput="Размер обуви"
                                             options={clothesSizes.shoes}
                                             size="form-control-sm"
                                             nameInput="ppiFootwear"
                                             valueInput={values.ppiFootwear}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.ppiFootwear}
                                             errorsInput={errors.ppiFootwear}
                                />
                            </div>

                            <div className="col-lg-3">
                                <SelectInput titleInput="Размер гол. убора"
                                             options={clothesSizes.headdress}
                                             size="form-control-sm"
                                             nameInput="ppiHeaddress"
                                             valueInput={values.ppiHeaddress}
                                             onBlurInput={handleBlur}
                                             onChangeInput={handleChange}
                                             touchedInput={touched.ppiHeaddress}
                                             errorsInput={errors.ppiHeaddress}
                                />
                            </div>

                            <div className="col-lg-2">
                                <Input titleInput="Рост"
                                       typeInput="text"
                                       size="form-control-sm"
                                       nameInput="ppiHeight"
                                       valueInput={values.ppiHeight}
                                       onBlurInput={handleBlur}
                                       onChangeInput={handleChange}
                                       touchedInput={touched.ppiHeight}
                                       errorsInput={errors.ppiHeight}
                                />
                            </div>
                        </Row>
                    </Row>

                    {/*Блок водительские удостоверения*/}
                    <Row className="mb-4">

                        <CardTitle className="h4">Водительские удостоверения</CardTitle>

                        <Row className="mb-3">
                            <div className="col-lg-3">
                        <Input titleInput="Категории"
                               typeInput="text"
                               size="form-control-sm"
                               nameInput="dlCategory"
                               valueInput={values.dlCategory}
                               onBlurInput={handleBlur}
                               onChangeInput={handleChange}
                               touchedInput={touched.dlCategory}
                               errorsInput={errors.dlCategory}
                        />
                            </div>
                        </Row>

                    </Row>


                    <Row>
                        <div className="text-center">
                            <Button type="submit" disabled={!(isValid && errors && dirty)} color="primary"
                                    className="btn-sm">Создать</Button>
                        </div>
                    </Row>

                </Form>
            )}
        </Formik>

    )
}
export default FormEmployee;