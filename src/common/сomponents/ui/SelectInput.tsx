import React, {ChangeEvent, FC, FocusEvent} from "react";

interface IProps {
    titleInput: string;
    options: string[];
    size: string;
    valueInput: string;
    nameInput: string;
    onBlurInput: { (e: FocusEvent<any>): void; <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void; }
    onChangeInput: { (e: ChangeEvent<any>): void; <T = string | ChangeEvent<any>>(field: T): T extends ChangeEvent<any> ? void : (e: string | ChangeEvent<any>) => void; };
    touchedInput: boolean | undefined;
    errorsInput: string | undefined;
}

const SelectInput: FC<IProps> = ({
                                     titleInput,
                                     size, options,
                                     nameInput, valueInput,
                                     onChangeInput, onBlurInput,
                                     touchedInput, errorsInput
                                 }) => {

    const validClass = () => {

        if (touchedInput && errorsInput) {
            return {
                field: "form-control is-invalid",
                feedback: "invalid-feedback"
            }
        }
        if (touchedInput && !errorsInput) {
            return {
                field: "form-control is-valid",
                feedback: "d-none"
            }

        } else {
            return {
                field: "form-control",
                feedback: "d-none"
            }
        }
    }

    return (
        <div className="row mb-1">


            <div className="col-lg-11">

                <label
                    className="col-form-label"
                >
                    {titleInput}
                </label>

                <select className={[validClass().field, size].join(' ')}
                        onChange={onChangeInput}
                        value={valueInput || ''}
                        name={nameInput}
                        onBlur={onBlurInput}>
                    <option defaultValue="">Выбрать</option>
                    {
                        options.map((option: string, index: number) => {
                            return (
                                <option key={index} value={option}>{option}</option>
                            )
                        })
                    }
                </select>
                {<p className={validClass().feedback}>{errorsInput}</p>}
            </div>
        </div>
    )
}

export default SelectInput;