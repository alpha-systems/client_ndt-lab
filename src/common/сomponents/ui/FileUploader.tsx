import React, {FC, FocusEvent} from "react";
import {FormikErrors, FormikTouched} from "formik";

interface IProps {
    titleInput: string;
    typeInput: string;
    size: string;
    setValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
    valueInput: any;
    nameInput: string;
    onBlurInput: { (e: FocusEvent<any>): void; <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void; }
    touchedInput: boolean | FormikTouched<any> | FormikTouched<any>[] | undefined;
    errorsInput: string | string[] | FormikErrors<any> | FormikErrors<any>[] | undefined;
}

const Input: FC<IProps> = ({
                               titleInput,
                               typeInput,
                               size,
                               nameInput,
                               valueInput,
                               setValue,
                               onBlurInput,
                               errorsInput
                           }) => {

    const validClass = () => {
        if (errorsInput) {
            return {
                field: "form-control is-invalid",
                feedback: "invalid-feedback d-block"
            }
        }
        if (valueInput !== null && !errorsInput) {
            return {
                field: "form-control is-valid",
                feedback: "d-none"
            }
        } else {
            return {
                field: "form-control",
                feedback: "d-none"
            }
        }
    }

    return (
        <div className="row mb-2">

            <div className="col-lg-11">

                <label
                    className=" col-form-label"
                >
                    {titleInput}
                </label>

                <input
                    className={validClass().field + " " + size}
                    name={nameInput}
                    type={typeInput}
                    onChange={(event) => {
                        const target = event.target as HTMLInputElement;
                        const file: File = (target.files as FileList)[0];
                        setValue(nameInput, file)
                    }}
                    onBlur={onBlurInput}
                    multiple
                />

                {<p className={validClass().feedback}>{errorsInput}</p>}

            </div>

        </div>
    )
}

export default Input;