import React, {FocusEvent, ChangeEvent, FC} from "react";

interface IProps {
    titleInput: string;
    typeInput: string;
    size: string;
    valueInput: string;
    nameInput: string;
    onBlurInput: {(e: FocusEvent<string>): void; <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void;}
    onChangeInput: {(e: ChangeEvent<any>): void; <T = string | ChangeEvent<any>>(field: T): T extends ChangeEvent<any> ? void : (e: string | ChangeEvent<any>) => void;};
    touchedInput: boolean | undefined;
    errorsInput: string | undefined;
}

const Input: FC<IProps> = ({
                               titleInput, typeInput,
                               size, nameInput,
                               valueInput, onChangeInput,
                               onBlurInput, touchedInput,
                               errorsInput
                           }) => {

    const validClass = () => {

        if (touchedInput && errorsInput){
            return {
                field: "form-control is-invalid",
                feedback: "invalid-feedback"
            }
        }
        if (valueInput !== '' && !errorsInput){
            return {
                field: "form-control is-valid",
                feedback: "d-none"
            }
        } else {
            return {
                field: "form-control",
                feedback: "d-none"
            }
        }
    }

    return (
        <div className="row mb-1">

            <div className="col-lg-11">
                <label
                    className=" col-form-label"
                >
                    {titleInput}
                </label>

                <input
                    className={validClass().field + " " + size}
                    type={typeInput}
                    onChange={onChangeInput}
                    value={valueInput || ''}
                    name={nameInput}
                    onBlur={onBlurInput}
                />
                {<p className={validClass().feedback}>{errorsInput}</p>}
            </div>
        </div>
    )
}

export default Input;