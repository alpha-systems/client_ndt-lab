import React, {useEffect} from 'react';
import MetaTags from 'react-meta-tags';
import {Card, CardBody, Col, Container, Row, Spinner} from "reactstrap";
import Breadcrumbs from "../../common/сomponents/ui/Breadcrumbs";
import {useDispatch, useSelector} from "react-redux";
import {fetchEmployees} from "../../store/Employee/actions";
import {employeesListSelector, errorsSelector, loadingSelector} from '../../store/Employee/selectors';
import BasicTable from "../../common/сomponents/employee/BasicTable";


const ListEmployees: React.FC = () => {
    const employees = useSelector(employeesListSelector);
    const loading = useSelector(loadingSelector);
    const error = useSelector(errorsSelector);
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(fetchEmployees());
    }, [dispatch])

    return (
        <>
            <div className="page-content">
                <MetaTags>
                    <title>Реестр сотрудников</title>
                </MetaTags>
                <Container fluid={true}>
                    <Breadcrumbs title="Сотрудники" breadcrumbItem="Реестр сотрудников"/>

                    <Row>
                        <Col md={12}>
                            <Card>
                                <CardBody>
                                    {
                                        loading ?
                                            <div className="text-center">
                                                <Spinner className="ms-2" color="primary"/>
                                            </div>
                                            :
                                            <BasicTable employees={employees}/>
                                    }

                                </CardBody>
                            </Card>
                        </Col>
                    </Row>

                </Container>


            </div>
        </>
    );
};

export default ListEmployees;
