import React, {FC} from "react";
import MetaTags from 'react-meta-tags';
import {Card, CardBody, Col, Container, Row,} from "reactstrap";
import Breadcrumbs from "../../common/сomponents/ui/Breadcrumbs";
import FormEmployee from "../../common/сomponents/employee/FormEmployee";


const createEmployee: FC = () => {


    return (
        <div className="page-content">
            <MetaTags>
                <title>Новый сотрудник</title>
            </MetaTags>
            <Container fluid={true}>
                <Breadcrumbs title="Сотрудники" breadcrumbItem="Новый сотрудник"/>

                <Row>
                    <Col md={12}>
                        <Card>
                            <CardBody>
                                <FormEmployee/>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

            </Container>


        </div>
    )
}

export default createEmployee;