import React from "react";
import {Route, Switch,} from "react-router-dom";
import {Router} from "react-router";
import {authRoutes, publicRoutes} from "./index";
import {useSelector} from "react-redux";
import {isAuthSelector} from "../store/Auth/selectors";
import history from "./history";

const AppRouter = () => {
    const userIsAuth = useSelector(isAuthSelector);

    return (
        userIsAuth
            ?
            <Switch>
                <Router history={history}>
                    {
                        authRoutes.map(route =>
                            <Route path={route.path}
                                   exact={route.exact}
                                   component={route.component}
                                   key={route.path}
                            />
                        )

                    }
                </Router>
            </Switch>
            :
            <Switch>
                <Router history={history}>
                    {
                        publicRoutes.map(route =>
                            <Route path={route.path}
                                   exact={route.exact}
                                   component={route.component}
                                   key={route.path}
                            />
                        )
                    }
                </Router>
            </Switch>
    )
}

export default AppRouter;