import React from "react";
import CreateEmployee from "../pages/Employee/CreateEmployee";
import EmployeesList from "../pages/Employee/ListEmployees";
import Employee from "../pages/Employee/Employee";

export interface IRoute {
    path: string;
    component: React.ComponentType;
    exact?: boolean;
}

export enum RoutesNames {
    ALL_EMPLOYEES = '/employees',
    NEW_EMPLOYEE = '/createEmployee',
    EMPLOYEE = '/employee/:id'
}

export const publicRoutes: IRoute[] = [
]

export const authRoutes: IRoute[] = [
    {path: RoutesNames.ALL_EMPLOYEES, exact: true, component: EmployeesList},
    {path: RoutesNames.NEW_EMPLOYEE, exact: true, component: CreateEmployee},
    {path: RoutesNames.EMPLOYEE, exact: true, component: Employee},
]