import {IEmployee, IHistory} from "./IEmployee";
import IFile from "../IFile";

export interface EmployeeState {
    employees: IEmployee[] ;
    loading: boolean;
    error: null | string;
}

// Данный интерфейс используется в инициализации employee для формы библиотеки Formik
export interface IInitialEmployee extends IHistory{
    photo: IFile | null;
}