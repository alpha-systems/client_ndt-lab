export interface IHistory {
    photoId?: number;
    historyId?: number;
    photoFileName?: string;
    firstName: string;
    patronymic: string;
    lastName: string;
    firstNameGenitive: string;
    patronymicGenitive: string;
    lastNameGenitive: string;
    firstNameDative: string;
    patronymicDative: string;
    lastNameDative: string;
    firstNameTransliteration: string;
    lastNameTransliteration: string;
    dateBirth: string;
    inn: string;
    gender: string;
    position: string;
    contractualEntity: string;
    phoneNumber1: string;
    phoneNumber2: string;
    email: string;
    passSeries: string;
    passNumber: string;
    passDateIssue: string;
    passIssuedBy: string;
    educationalGrounding: string;
    institutionOfLearning: string;
    speciality: string;
    addressPostcode: string;
    addressCountry: string;
    addressRegion: string;
    addressSettlement: string;
    addressStreet: string;
    addressHouse: string;
    addressBlock: string;
    addressFlat: string;
    ppiClothing: string;
    ppiHeaddress: string;
    ppiFootwear: string;
    ppiHeight: string;
    ppiPants: string;
    ppiGloves: string;
    dlCategory: string;
    beginDate?: string;
    naviCreateUser: number;
    naviUpdateUser?: string;
}

export interface IEmployee {
    employeeId: number;
    historyList: IHistory[];
}




