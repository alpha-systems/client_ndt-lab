import axios from "axios";

class Api {
    private DEV_URL = 'http://localhost:8080/api'

    async get(url: string, params?:any){
        return axios.get(this.DEV_URL + url, params)
    }

    async post(url: string, data: any){
        return axios.post(this.DEV_URL + url, data)
    }

    async postFile(url: string, data:any){
        return axios.post(this.DEV_URL + url, data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    }

}

export default Api;
