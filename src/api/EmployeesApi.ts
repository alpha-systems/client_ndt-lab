import Api from "./Api"

class EmployeesApi extends Api {

    getEmployeesList(params?: any) {
        return this.get('/employees', params);
    }
    //TODO Добавить тип
    addEmployee(data: any) {
        return this.post( '/employee', data);
    }

    uploadPhotoEmployee(file: any){

        let formData = new FormData();

        formData.append('file', file)

        return this.postFile('/employeePhoto', formData )

    }
}

export default new EmployeesApi();